#!/usr/bin/env python
# -*- coding: utf-8 -*-

from reportlab.graphics.shapes import Drawing,String
from reportlab.graphics import renderPDF

#创建一个指定尺寸的Drawing对象
d=Drawing(100,100)

#再创建具有指定属性的图形元素（这里是一个String对象）
s=String(50,50,'Hello World',textAnchor='middle')
#将图形元素添加到Drawing对象中
d.add(s)

#以PDF格式渲染Drawing对象,并将结果保存到文件中
renderPDF.drawToFile(d,'hello.pdf','A simple PDF file')


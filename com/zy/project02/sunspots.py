#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
最终的太阳黑子程序
'''

from reportlab.graphics.shapes import *
from reportlab.graphics.charts.lineplots import LinePlot
from reportlab.graphics.charts.textlabels import Label
from reportlab.graphics import renderPDF


COMMENT_CHARS = '#:'
#创建一个指定尺寸的Drawing对象
drawing=Drawing(400,200)
data=[]
file_path="../../../file_data/Predict.txt"
#从文件中提取数据
with open(file_path,'r+') as f:
    for line in f :
        if not line.isspace() and line[0] not in COMMENT_CHARS:
            data.append([float(n) for n in line.split()])

pred = [row[2] for row in data]
high = [row[3] for row in data]
low = [row[4] for row in data]
times = [row[0] + row[1]/12.0 for row in data]

lp=LinePlot()
lp.x=50
lp.y=50
lp.height=125
lp.width=300
lp.data=[list(zip(times, pred)),list(zip(times, high)),list(zip(times, low))]

#为每条线设置不同的颜色
lp.lines[0].strokeColor= colors.blue
lp.lines[1].strokeColor = colors.red
lp.lines[2].strokeColor = colors.green

drawing.add(lp)
#类似于标题
drawing.add(String(250,150,'Sunspots',fontSize=14,fillColor=colors.red))

renderPDF.drawToFile(drawing, 'report2.pdf', 'Sunspots')

# C:\Users\Administrator\AppData\Local\Programs\Python\Python36\python.exe
# -*- coding: utf-8 -*-
import tkinter as tk

class GUITest(tk.Frame):
    def __init__(self,master):
        super().__init__(master)
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.input=input=tk.Entry(self)
        input.pack(side='left')
        self.submit=submit=tk.Button(self)
        submit['text']="Fetch"
        submit['command']=self.fetch_handler
        submit.pack()
        self.add_Listbox()
    def fetch_handler(self):
        print('123')
    def add_Listbox(self):
        var2 = tk.StringVar()  # 定义变量
        var2.set((11, 22, 33, 44))
        lb = tk.Listbox(self, listvariable=var2)
        lb.pack()
        list_items = [1, 2, 3, 4]
        for item in list_items:  # 将这个列表全部输出到lb
            lb.insert('end', item)  # 用end方式添加



if __name__=='__main__':
    root=tk.Tk()
    root.title("File Sharing Client")
    client=GUITest(root)
    client.mainloop()
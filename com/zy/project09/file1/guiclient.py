# C:\Users\Administrator\AppData\Local\Programs\Python\Python36\python.exe
# -*- coding: utf-8 -*-


from xmlrpc.client import ServerProxy, Fault
from server import Node, UNHANDLED
from client import random_string
from threading import Thread
from time import sleep
from os import listdir
import sys
import tkinter as tk

"""
支持GUI的P2P文件共享程序
"""

HEAD_START = 0.1 # 单位为秒
SECRET_LENGTH = 100


class ListabNode(Node):
    def list(self):
        return listdir(self.dirname)


class Client(tk.Frame):
    def __init__(self,master,url,dirname,urlfile):
        super().__init__(master)
        self.node_setup(url,dirname,urlfile)
        self.pack()
        self.create_widgets()

    def node_setup(self,url,dirname,urlfile):
        self.secret=random_string(SECRET_LENGTH)
        n=ListabNode(url,dirname,urlfile)
        t=Thread(target=n._start)
        t.setDaemon(1)
        t.start()

        # 让服务器先行一步：
        sleep(HEAD_START)
        self.server = ServerProxy(url)
        for line in open(urlfile):
            line=line.strip()
            self.server.hello(line)
    def create_widgets(self):
        self.input=input=tk.Entry(self)
        input.pack()

        self.submit=submit=tk.Button(self)
        submit['text']="Fetch"
        submit['command']=self.fetch_handler
        submit.pack()

        self.files=files=tk.Listbox(self)
        files.pack(side='bottom',expand=True,fill=tk.BOTH)
        self.update_list()

    def fetch_handler(self):
        query=self.input.get()
        try:
            self.server.fetch(query, self.secret)
            self.update_list()
        except Fault as f:
            if f.faultCode!=UNHANDLED: raise
            print("Couldn't find the file", query)
    #刷新，Listbox 文件列表
    def update_list(self):
        self.files.delete(0,tk.END)
        self.files.insert(tk.END,self.server.list())

def main():
    urlfile, directory, url = sys.argv[1:]
    root=tk.Tk()
    root.title("File Sharing Client")
    client=Client(root, url, directory, urlfile)
    client.mainloop()

if __name__ == '__main__': main()

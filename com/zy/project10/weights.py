# C:\Users\Administrator\AppData\Local\Programs\Python\Python36\python.exe
# -*- coding: utf-8 -*-


'''
    简单的“铅锤从天而降”动画
'''

import sys,pygame
from random import randrange
from pygame.locals import *


class Weight(pygame.sprite.Sprite):
    def __init__(self,speed):
        pygame.sprite.Sprite.__init__(self)
        self.speed=speed
        #绘制Sprite对象时要用到的图像和矩形：
        self.image=weight_image
        self.rect=self.image.get_rect()
        self.reset()

    def reset(self):
        """
       将铅锤移到屏幕顶端的一个随机位置
        """
        self.rect.top=-self.rect.height
        self.rect.centerx=randrange(screen_size[0])

    def update(self):
        """
       更新下一帧中的铅锤
        """
        if self.rect.top> screen_size[1]:
            self.reset()
        self.rect.top += self.speed


#初始化
pygame.init()
screen_size=1366,768
pygame.display.set_mode(screen_size,FULLSCREEN)
pygame.mouse.set_visible(0)

# 加载铅锤图像
image_path="images/jack.jpg"
weight_image=pygame.image.load(image_path)
weight_image=weight_image.convert() # 以便与显示匹配

# 你可能想设置不同的速度
#speed=5

# 创建一个Sprite对象编组，并在其中添加一个Weight实例
sprites = pygame.sprite.RenderUpdates()
#同时添加三个铅块
sprites.add(Weight(speed=2))
sprites.add(Weight(speed=3))
sprites.add(Weight(speed=5))

# 获取并填充屏幕表面
screen=pygame.display.get_surface()
bg=(255, 255, 255) #白色
#以白色填充屏幕表面
screen.fill(bg)
#显示所做的修改
pygame.display.flip()


clock=pygame.time.Clock() # 设置时钟，限制移动速度

# 用于清除Sprite对象：
def clear_back(surf,rect):
    surf.fill(bg, rect)

while True:
    clock.tick(60)  # 然后在while循环中设置多长时间运行一次，每秒执行60次
    # 检查退出事件：
    for event in pygame.event.get():
        if event.type==QUIT:
            sys.exit()
        if event.type== KEYDOWN and event.key == K_ESCAPE:
            sys.exit()
    # 清除以前的位置：
    sprites.clear(screen,clear_back)
    # 更新所有的Sprite对象：
    sprites.update() #方法update调用Weight实例的方法update
    # 绘制所有的Sprite对象：
    updates=sprites.draw(screen) #调用sprites.draw并将屏幕表面作为参数，以便在当前位置绘制铅锤
    # 更新必要的显示部分：
    pygame.display.update(updates)
# C:\Users\Administrator\AppData\Local\Programs\Python\Python36\python.exe
# -*- coding: utf-8 -*-

'''
    这个模块的内容不属于项目中的，是小编无意间看到的一篇博客，
    但是对于理解pygame有很大的帮助，所以就实现以下看看
'''

'''
这里我们需要的pygame的模块有：
pygame.display  访问显示设备
pygame.draw     绘制形状、线和点
pygame.event    管理事件
pygame.image    加载和存储图片
pygame.rect     管理矩形区域
pygame.sprite   操作移动图像
pygame.surface  管理图像和屏幕
pygame.time     管理时间和帧信息
'''

import pygame
import sys
#调用pygame中的所有常量，便于使用
from pygame.locals import *



pygame.init()# 初始化pygame
screen_size=640, 480 # 设置窗口大小
screen=pygame.display.set_mode(screen_size)  # 显示窗口
screen_bg=0,0,0

#加载游戏图片(画一个小球)
color = 255,255,0
x,y = 100,100
radius = 10
width=10

move_x,move_y = [5, 5]  # 设置移动的X轴、Y轴


#clock = pygame.time.Clock()  # 设置时钟(由于设置小球的移动速度)


while True:# 死循环确保窗口一直显示
   # clock.tick(200)  # 每秒执行60次
    for event in pygame.event.get(): # 遍历所有事件
        if event.type== pygame.QUIT: # 如果单击关闭窗口，则退出
            sys.exit()
        screen.fill(screen_bg) # 填充屏幕颜色
        #screen.blit(ball,ballrect) # #在屏幕中画一个圆
        x+=move_x
        y+=move_y
        pygame.draw.circle(screen, color, (x,y), radius, width)
        #碰撞检测
        if(x<0 or x>640):
            move_x=-move_x
        if(y<0 or y>480):
            move_y=-move_y

        pygame.display.flip()

pygame.quit()  # 退出pygame


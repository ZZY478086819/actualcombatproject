# C:\Users\Administrator\AppData\Local\Programs\Python\Python36\python.exe
# -*- coding: utf-8 -*-

from xmlrpc.client import ServerProxy
from os.path import join,isfile
from xmlrpc.server import SimpleXMLRPCServer
from urllib.parse import urlparse
import sys

#历史记录的长度不超过6是因为，网络中的任何对等体最多通过6步就能到达其他任何对等体
MAX_HISTORY_LENGTH=6
OK=1
FAIL=2
EMPTY=''

def get_port(url):
    '从URL中提取端口'
    name=urlparse(url)[1]
    parts=name.split(':')
    return int(parts[-1])

#node
class Node:
    """
     P2P网络中的节点
     """
    def __init__(self,url,dirname,secret):
        self.url=url #可能加入到查询历史记录中或提供给其他节点
        self.dirname=dirname #可访问的目录
        self.secret=secret #密码
        self.known=set() #已知对等体集合
    def hello(self,other):
        """
        用于向其他节点介绍当前节点
         """
        self.known.add(other)
        return OK

    '''
        
        history:历史记录包含一系列不应再向其查询的URL，因为它们正在等待该查询的响应
    '''
    def query(self,query,history=[]):
        """
            查询文件（可能向已知节点寻求帮助），并以字符串的方式返回它
        """
        code,data=self._handler(query)
        if code==OK:
            return code,data
        else:
            history=history+[self.url]
            if len(history) >=MAX_HISTORY_LENGTH:
                return FAIL,EMPTY
            return  self._broadcast(query, history)

    def fetch(self,query,secret):
        """
            用于让节点查找并下载文件
        """
        if secret !=self.secret:return FAIL
        code,data=self.query(query)
        if code==OK:
            f=open(join(self.dirname,query),'w')
            f.write(data)
            f.close()
            return OK
        else:
            return FAIL

    #以下划线打头，意味着不能通过XML-RPC来访问它们
    def _handler(self,query):
        """
       供内部用来处理查询
        """
        dir=self.dirname
        name=join(dir,query)
        if not isfile(name):return FAIL,EMPTY
        return OK,open(name).read()

    def _broadcast(self,query, history):
        """
       供内部用来向所有已知节点广播查询
        """
        for other in self.known.copy(): #不应直接迭代self.known本身，因为这个集合在迭代期间可能被修改。使用其副本更安全
            if other in history: continue #这句话的意思的是，跳过当前对等体，也就是不能处理这个URL的对等体
            try:
                s=ServerProxy(other)
                code,data=s.query(query,history)
                if code==OK:
                    return code, data
            except:
                self.known.remove(other)
        return FAIL, EMPTY
    def _start(self):
        """
       供内部用来启动XML-RPC服务器
        """
        port=get_port(self.url)
        s=SimpleXMLRPCServer(("", port),logRequests = False)
        s.register_instance(self)
        s.serve_forever()
        #程序入口
def main():
   url,directory,secret=sys.argv[1:]
   n=Node(url,directory,secret)
   n._start()
if __name__ == '__main__':
   main()


# C:\Users\Administrator\AppData\Local\Programs\Python\Python36\python.exe
# -*- coding: utf-8 -*-

'''
对下面案例的解释：
方法register_instance注册一个实现了其“远程方法”的实例，也
可使用方法register_function注册各个函数。为运行服务器做好准备（让它能够响应来自外部的
请求）后，调用其方法serve_forever ，方法serve_forever解释器看起来就像“挂起”了一样，但实际上它是在等待RPC请求。
'''
from xmlrpc.server import SimpleXMLRPCServer
s=SimpleXMLRPCServer(("",16888)) #localhost和端口4242
def twice(x):
    return x*2
s.register_function(twice)  # 给服务器添加功能,也就是客户端调用的函数
s.serve_forever() # 启动服务器

#!/usr/bin/env python
# -*- coding: utf-8 -*-


'''

添加一些标记
此时我们需要根据需求，添加一些标签：
(1) 打印一些起始标记。
(2) 对于每个文本块，在段落标签内打印它。
(3) 打印一些结束标记。

这里假设要将第一个文本块放在一级标题标签（h1）内，而不是段
落标签内。另外，还需将用星号括起的文本改成突出文本（使用标签em）。这样程序将更有用一些。
由于已经编写好了函数blocks，使用re.sub实现这些需求的代码非常简单
'''

import re
#引用刚刚编写的util模块
from project01.util import *

print('<html><head><title>zzy-python</title><body>')
title = True
file='../../file_data/test_input.txt'
#for block in blocks(sys.stdin) 这里可以使用标准的输入，小编为了方便运行，就本地读取
with open(file) as f:
    for block in blocks(f):
        re.sub(r'\*(.+?\*)',r'<em>\1</em>',block)
        if title:
            print('<h1>')
            print(block)
            print('</h1>')
            title=False
        else:
            print('<p>')
            print(block)
            print('</p>')
print('</body></html>')

'''
到这里简单的实现就完成了，但是如果要扩展这个原型，该如何办呢？可在for循环中添加检查，以确定文本块是否是标题、
列表项等。为此，需要添加其他的正则表达式，代码可能很快变得很乱。更重要的是，要让程序
输出其他格式的代码（而不是HTML）很难，但是这个项目的目标之一就是能够轻松地添加其他
输出格式。这里假设你要重构这个程序，以采用稍微不同的结构。
'''

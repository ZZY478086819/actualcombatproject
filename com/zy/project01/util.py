#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''

首先要做的事情之一是将文本分成段落。从准备测试文件中可以看出，段落之间有一个或多个空行。
比段落更准确的说法是块（block），因为块也可以指标题和列表项。

而这个模块实现的就是找出文本块：
要找出这些文本块，一种简单的方法是，收集空行前的所有行并将它们返回，然后重复这样
的操作。不需要收集空行，因此不需要返回空文本块（即多个空行）。另外，必须确保文件的最
后一行为空行，否则无法确定最后一个文本块到哪里结束。
'''

#生成器lines是个简单的工具，在文件末尾添加一个空行
def lines(file):
    for line in file:
        yield line
    yield '\n'

# 生成器blocks实现了刚才描述的方法。生成文本块时，将其包含的所有行合并，
#并将两端多余的空白（如列表项缩进和换行符）删除，得到一个表示文本块的字符串。
def blocks(file):
    block=[]
    for line in lines(file):
        if line.strip():
            block.append(line)
        elif block:
            yield ''.join(block).strip()
            block=[]


if __name__=='__main__':
    file='../../file_data/test_input.txt'
    with open(file,'r+') as f :
        for line in blocks(f):
            print(line)
#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
网站生成器
'''

from xml.sax.handler import ContentHandler
from xml.sax import parse
import os

class Dispatcher:
    #根据前缀（'start'或'end'）和标签名（如'page'），生成处理程序的名称（如'startPage'）
    def startElement(self,name,attrs):
        self.dispatch('start', name, attrs)
    def endElement(self,name):
        self.dispatch('end',name)

    def dispatch(self,prefix,name,attrs=None):
        #根据前缀（'start'或'end'）和标签名（如'page'），生成处理程序的名称（如'startPage'）
        mname=prefix+name.capitalize()
        #根据前缀生成默认处理程序的名称（如'defaultStart'）
        dname='default'+prefix.capitalize()
        #尝试使用getattr获取处理程序，并将默认值设置为None
        method=getattr(self,mname,None)
        if callable(method) :args=() #如果结果是可调用的，就将args设置为一个空元组
        else:#否则，就尝试使用getattr获取默认处理程序，并将默认值也设置为None
            method = getattr(self, dname, None)
            args = (name,) #将args设置为一个只包含标签名的元组
        #如果要调用的是起始处理程序，就将属性添加到参数元组（args）中
        if prefix=='start':args+=attrs,  #注意这里一定要是一个元组
        #如果获得的处理程序是可调用的，就使用正确的参数调用它
        if callable(method):method(*args)

class WebsiteConstructor(Dispatcher,ContentHandler):
    passthrough = False #判断是否在标签内

    #初始化操作
    def __init__(self,directory):
        self.directory=[directory] #存放html文件的目录
        self.ensureDirectory()  #首次创建目录
    #创建目录
    def ensureDirectory(self):
        path=os.path.join(*self.directory)  #拼接目录结构
        os.makedirs(path,exist_ok=True)  #如果存在不做任何操作

    def characters(self,chars):
        if self.passthrough: self.out.write(chars)

    #创建默认的处理程序(主要用于处理，非page、director 标签)
    def defaultStart(self,name,attrs):
        if self.passthrough: #获取所有属性
            self.out.write('<' + name)
            for key, val in attrs.items():
                self.out.write(' {}="{}"'.format(key, val))
            self.out.write('>')

    def defaultEnd(self,name):
        if self.passthrough:
            self.out.write('</{}>'.format(name))

    #用于获取director，创建相应的目录
    def startDirectory(self,attrs):
        self.directory.append(attrs['name'])
        self.ensureDirectory()
    #如果是director 标签的结尾，就将该标签的路径，从路径列表中弹出
    def endDirectory(self):
        self.directory.pop()

    #用于获取page，创建相应的文件对象，以及标记在page标签内，同一个page中的内容写入一个html
    def startPage(self,attrs):
        filename = os.path.join(*self.directory + [attrs['name'] + '.html'])
        self.out = open(filename, 'w')  #获取文件写对象
        self.writeHeader(attrs['title'])
        self.passthrough = True  #设置在page标签内
    #获取到page的结束标签，表示该页面内容获取结束
    def endPage(self):
        self.passthrough=False #设置不在page标签内
        self.writeFooter()  #写入页面尾
        self.out.close() #关闭此页面文件对象

        #用于写入页面的开头和结尾
    def writeHeader(self,title):
        self.out.write('<html>\n <head>\n <title>')
        self.out.write(title)
        self.out.write('</title>\n </head>\n <body>\n')
    def writeFooter(self):
        self.out.write('\n </body>\n</html>\n')


#开始解析
file_path='../../../file_data/website.xml'
parse(file_path,WebsiteConstructor('public_html'))

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from xml.sax.handler import ContentHandler
from xml.sax import parse


'''
这个模块主要完成：
简单的解析这个XML，提取有用信息，重新格式化为HTML格式，
最终根据不同page写入不同的HTML文件中
'''

class PageMaker(ContentHandler):
    #跟踪是否在标签内部
    passthrough = False

    #标签的开始
    def startElement(self,name,attrs):
        if name=='page':
            self.passthrough=True
            self.out= open(attrs['name'] + '.html', 'w')  #创建输出到的HTML文件的名称
            self.out.write('<html><head>\n')
            #name="index" title="Home Page"
            #attrs['title']提取标签中属性的key-value
            self.out.write('<title>{}</title>\n'.format(attrs['title']))
            self.out.write('</head><body>\n')
        elif self.passthrough:  #如果标签下有嵌套的子标签
            self.out.write('<' + name)
            for key,val in attrs.items(): #获取所有属性
                self.out.write(' {}="{}"'.format(key, val))
            self.out.write('>')

    #标签的结束
    def endElement(self, name):
        if name=='page':
            self.passthrough = False
            self.out.write('\n</body></html>\n')
            self.out.close()
        elif self.passthrough:
            self.out.write('</{}>'.format(name))

    #标签中的内容比如：<h1>123</h1> --- > 123
    def characters(self, content):
        if self.passthrough:self.out.write(content)
file_path='../../../file_data/website.xml'

#解析
parse(file_path,PageMaker())



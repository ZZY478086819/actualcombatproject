# C:\Users\Administrator\AppData\Local\Programs\Python\Python36\python.exe
# -*- coding: utf-8 -*-

print('Content-type: text/html\n')

import cgitb;cgitb.enable()
import pymysql

server_host = "localhost"
user = "root"
passwd = "123456"
db_name = "test"
# 打开数据库连接
conn = pymysql.connect(server_host, user, passwd, db_name)
# 使用 cursor() 方法创建一个游标对象 cursor
curs = conn.cursor()

import cgi, sys
form = cgi.FieldStorage()
sender=form.getvalue('sender')
subject=form.getvalue('subject')
text=form.getvalue('text')
reply_to=form.getvalue('reply_to')
import re
pat=r'\'|"'
text=re.sub(pat,'_',text)
if not (sender and subject and text):
    print('Please supply sender, subject, and text')
    sys.exit()
if reply_to is not None:
    query=("""
    INSERT INTO messages(reply_to, sender, subject, text) VALUES (%s,'%s','%s','%s')
    """%(reply_to,sender,subject,text))
else:
    query=("""
    INSERT INTO messages(sender, subject, text)  VALUES('%s', '%s', '%s')
    """%(sender,subject,text))

curs.execute(query)
conn.commit()

print("""
<html> 
<head>
 <title>Message Saved</title>
</head>
<body>
 <h1>Message Saved</h1>
 <hr />
 <a href='main.py'>Back to the main page</a>
</body>
</html>s
""")

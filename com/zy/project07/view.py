# C:\Users\Administrator\AppData\Local\Programs\Python\Python36\python.exe
# -*- coding: utf-8 -*-

print('Content-type: text/html\n')
import cgitb;

cgitb.enable()

import pymysql
import cgi, sys

server_host = "localhost"
user = "root"
passwd = "123456"
db_name = "test"
# 打开数据库连接
conn = pymysql.connect(server_host, user, passwd, db_name)
# 使用 cursor() 方法创建一个游标对象 cursor
curs = conn.cursor()
form = cgi.FieldStorage()
id = form.getvalue('id')
print("""
<html>
 <head>
 <title>View Message</title> 
  </head>
 <body>
 <h1>View Message</h1>
 """)
try:
    id=int(id)
except:
    print('Invalid message ID')
    sys.exit()
curs.execute('SELECT * FROM messages WHERE id = %s', (format(id),))
rows=curs.fetchall()

if not rows:
    print('Unknown message ID')
    sys.exit()

row=rows[0]
print("""
 <p><b>Subject:</b> {subject}<br />
 <b>Sender:</b> {sender}<br />
 <pre>{text}</pre>
 </p>
 <hr />
 <a href='main.py'>Back to the main page</a>
 | <a href="edit.py?reply_to={id}">Reply</a>
 </body>
</html>
""".format(subject=row[1],sender=row[2],text=row[4],id=row[0]))

# C:\Users\Administrator\AppData\Local\Programs\Python\Python36\python.exe
# -*- coding: utf-8 -*-

import pymysql

def create_table(cursor,table_name,create_sql):
    # 使用 execute() 方法执行 SQL，如果表存在则删除
    drop_table = 'drop table if EXISTS '+table_name
    cursor.execute(drop_table)
    # 建表
    cursor.execute(sql)

def insert(cursor,table_name):
    reply_to = input('Reply to: ')
    subject = input('Subject: ')
    sender = input('Sender: ')
    text = input('Text: ')
    sql='insert into {}(reply_to, sender, subject, text) values({},"{}","{}","{}")'\
        .format(table_name,reply_to, sender, subject, text)
    cursor.execute(sql)


if __name__=='__main__':
    server_host="localhost"
    user="root"
    passwd= "123456"
    db_name="test"
    # 打开数据库连接
    db = pymysql.connect(server_host, user,passwd,db_name)
    # 使用 cursor() 方法创建一个游标对象 cursor
    cursor = db.cursor()
    #建表
    sql = '''
    CREATE TABLE messages (
     id INT NOT NULL AUTO_INCREMENT,
     subject VARCHAR(100) NOT NULL,
     sender VARCHAR(15) NOT NULL,
     reply_to INT,
     text MEDIUMTEXT NOT NULL, PRIMARY KEY(id)
    )
    '''
    #create_table(cursor,"messages",sql)
    #插入数据
    #insert(cursor,db_name+".messages")
    #db.commit()
    cursor.execute('SELECT * FROM test.messages')
    rows = cursor.fetchall()
    for row in rows:
        print(type(row[3]))
        print(row[3])
    # 关闭连接
    cursor.close()
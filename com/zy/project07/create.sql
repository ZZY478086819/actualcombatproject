--创建PostgreSQL数据库
CREATE TABLE messages (
 id SERIAL PRIMARY KEY, -- 用于标识消息 主键
 subject TEXT NOT NULL, -- 包含消息主题的字符串。
 sender TEXT NOT NULL,  -- 包含发送者姓名、电子邮箱地址或其他类似信息的字符串。
 reply_to INTEGER REFERENCES messages, -- 如果消息是另一条消息的回复，这个字段将包含那条消息的id，否则为空。
 text TEXT NOT NULL -- 包含消息正文的字符串。
);

-- 创建MySQL数据库
CREATE TABLE messages (
 id INT NOT NULL AUTO_INCREMENT,
 subject VARCHAR(100) NOT NULL,
 sender VARCHAR(15) NOT NULL,
 reply_to INT,
 text MEDIUMTEXT NOT NULL, PRIMARY KEY(id)
);

-- 创建SQLite数据库
create table messages (
 id integer primary key autoincrement,
 subject text not null,
 sender text not null,
 reply_to int,
 text text not null
);

-- 插入语句
insert into test.messages(reply_to, sender, subject, text) values(3,"xxx@163.com","xiaolan","I love you !");
insert into test.messages(reply_to, sender, subject, text) values(4,"xxx@163.com","xiaohong","I hate you !");
insert into test.messages(sender, subject, text) values("xxx@163.com","xiaowang","I fuck you !");
insert into test.messages(sender, subject, text) values("xxx@163.com","xiaozi","I like you !");

insert into test.messages(id, sender, subject, text) values(3,"xxx@163.com","xiao99","I love you !");
insert into test.messages(id, sender, subject, text) values(4,"xxx@163.com","xiao88","I hate you !");

# C:\Users\Administrator\AppData\Local\Programs\Python\Python36\python.exe
# -*- coding: utf-8 -*-

'''
 公告板主页
'''

print('Content-type: text/html\n')
import cgitb;

cgitb.enable()

import pymysql

server_host = "localhost"
user = "root"
passwd = "123456"
db_name = "test"
# 打开数据库连接
db = pymysql.connect(server_host, user, passwd, db_name)
curs = db.cursor()

print("""
<html>
 <head>
 <title>The FooBar Bulletin Board</title>
 </head>
 <body>
 <h1>The FooBar Bulletin Board</h1> 
""")
toplevel = []
children = {}
curs.execute('SELECT * FROM test.messages')
rows = curs.fetchall()
for row in rows:
    parent_id = row[3]
    if parent_id is None:
        toplevel.append(row)
    else:
        children.setdefault(parent_id, []).append(row)

def format(row):
    print(row[1]+'</br>')
    try:
        kids = children[row[0]]
    except KeyError:
        pass
    else:
        print('<blockquote>')
        for kid in kids:
            format(kid)
        print('</blockquote>')

print('<p>')
for row in toplevel:
    format(row)
print("""
 </p>
 </body>
 </html>
 """)

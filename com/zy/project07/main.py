# C:\Users\Administrator\AppData\Local\Programs\Python\Python36\python.exe
# -*- coding: utf-8 -*-


print('Content-type: text/html\n')

import cgitb;

cgitb.enable()

import pymysql

server_host = "localhost"
user = "root"
passwd = "123456"
db_name = "test"
# 打开数据库连接
conn = pymysql.connect(server_host, user, passwd, db_name)
# 使用 cursor() 方法创建一个游标对象 cursor
curs = conn.cursor()

print("""
 <html>
 <head>
 <title>The FooBar Bulletin Board</title>
 </head>
 <body>
 <h1>The FooBar Bulletin Board</h1>
 """)
curs.execute('SELECT * FROM messages')
rows=curs.fetchall()

toplevel = []
children = {}

for row in rows:
    parent_id = row[3]
    if parent_id is None:
        toplevel.append(row)
    else:
        children.setdefault(parent_id, []).append(row)

def format(row):
    print('<p><a href="view.py?id={id}">{subject}</a></p>'.format(id=row[0],subject=row[1]))
    try:
        kids = children[row[0]]
    except KeyError:
        pass
    else:
        print('<blockquote>')
        for kid in kids:
            format(kid)
        print('</blockquote>')

print('<p>')
for row in toplevel:
    format(row)
print("""
 </p>
 <hr />
 <p><a href="edit.py">Post message</a></p>
 </body>
</html>
""")


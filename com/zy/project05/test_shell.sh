#!/usr/bin/env bash
#启动程序
python chatserver.py

#客户端连接
telnet localhost 5005

#聊天服务器支持的命令
命 令         可在什么地方使用         描 述
login name      LoginRoom         用于登录服务器
logout          所有聊天室          用于退出服务器
say statement   主聊天室            用于说话
look            主聊天室        用户确定聊天室内还有谁
who             主聊天室        用户确定谁登录了服务器
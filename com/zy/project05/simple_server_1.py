#!/usr/bin/env python
# -*- coding: utf-8 -*-

from asyncore import dispatcher
import socket,asyncore

'''
一个能够接受连接的服务器
'''

PORT=5005
NAME = 'TestChat'


'''
为创建简单的ChatServer类，可继承模块asyncore中的dispatcher类。dispatcher类基本上是
一个套接字对象，但还提供了一些事件处理功能。
'''
class ChatServer(dispatcher):
    '''
    一个接受连接并创建会话的类。它还负责向这些会话广播
    '''
    def __init__(self,port):
        dispatcher.__init__(self)
        #调用了create_socket，并通过传入两个参数指定了要创建的套接字类型,通常都使用这里使用的类型
        self.create_socket(socket.AF_INET,socket.SOCK_STREAM)
        '''
            调用了set_reuse_addr，让你能够重用原来的地址（具体地说是端口号）,
            即便未妥善关闭服务器亦如此。不会出现端口被占用情况
        '''
        self.set_reuse_addr()
        '''
            bind的调用将服务器关联到特定的地址（主机名和端口）。 
            空字符串表示：localhost，或者说当前机器的所有接口
        '''
        self.bind(('',port))
        #listen的调用让服务器监听连接；它还将在队列中等待的最大连接数指定为5。
        self.listen(5)
    def handle_accept(self):
        '''
        重写事件处理方法handle_accept，让它在服务器接受客户端连接时做些事情
        '''
        #调用self.accept，以允许客户端连接。
        #返回一个连接（客户端对应的套接字）和一个地址（有关发起连接的机器的信息）。
        conn,addr=self.accept()
        #addr[0]是客户端的IP地址
        print('Connection attempt from',addr[0])
if __name__=='__main__':
    s=ChatServer(PORT)
    try:
        #启动服务器的监听循环
        asyncore.loop()
    except KeyboardInterrupt:
        pass
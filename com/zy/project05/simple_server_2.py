#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
包含ChatSession类的服务器程序
'''

from asyncore import dispatcher
from asynchat import async_chat
import socket,asyncore

PORT=5005
NAME='TestChat'
class ChatSession(async_chat):
    def __init__(self,socket,server):
        async_chat.__init__(self,socket)
        #设置结束符，
        self.server=server
        self.set_terminator("\r\n")
        self.data=[]
        self.push(('Welcome to {}\r\n'.format(self.server.name)).encode())

    #从套接字读取一些文本
    def collect_incoming_data(self, data):
        self.data.append(data)


    #读取到结束符时将调用found_terminator
    def found_terminator(self):
        line=' '.join(self.data)
        self.data=[]
        #使用line做些事情……
        print(line)

class ChatServer(dispatcher):
    def __init__(self,port,name):
        dispatcher.__init__(self)
        self.name=name
        self.create_socket(socket.AF_INET,socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind(('',port))
        self.listen(5)
       #ChatServer存储了一个会话列表
        self.sessions=[]
    #接受一个新请求，就会创建一个新的ChatSession对象，并将其附加到会话列表末尾
    def handle_accept(self):
        conn,addr=self.accept()
        self.sessions.append(ChatSession(conn,self))

if __name__=='__main__':
    s=ChatServer(PORT,NAME)
    try:
        asyncore.loop()
    except KeyboardInterrupt as e :
        print(e)


#基本的命令解释功能,例如：say Hello, world!
class CommandHandler:
    '''
        类似于标准库中cmd.Cmd的简单命令处理程序
    '''
    #参数不正确
    def unknown(self,session,cmd):
        session.push('Unknown command: {}s\r\n'.format(cmd).encode())
    #根据命令，匹配方法，调用
    def handler(self,session,line):
        if not line.strip():return
        parts=line.split(' ',1)
        cmd=parts[0]
        try:
            line=parts[1].strip()
        except IndexError:
            line=''
        meth = getattr(self, 'do_' + cmd, None)
        try:
            meth(session,line)
        except TypeError:
            self.unknown(session,cmd)
    def do_say(self,session,line):
        session.push(line.encode())

#聊天室
class EndSession(Exception):pass
class Room(CommandHandler):
    """
    可包含一个或多个用户（会话）的通用环境。
    它负责基本的命令处理和广播
    """
    def __init__(self,server):
        self.server=server
        self.sessions=[]
    def add(self,session):
        self.sessions.append(session)
    def remove(self,session):
        self.sessions.remove(session)

    def broadcast(self,line):
        for session in self.sessions:
            session.push(line.encode())
    def do_logout(self,session,line):
        raise EndSession
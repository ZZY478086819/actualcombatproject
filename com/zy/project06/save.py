# C:\Users\Administrator\AppData\Local\Programs\Python\Python36\python.exe

print('Content-type: text/html\n\n')

from os.path import join,abspath
from hashlib import sha1
import cgi,sys


BASE_DIR = abspath('data')
form=cgi.FieldStorage()
text=form.getvalue('text')
filename=form.getvalue('filename')
password=form.getvalue('password')

#密码是123456
if not (text and filename and password):
    print('Invalid parameters.')
    sys.exit()
if (sha1(password.encode()).hexdigest()!= '7c4a8d09ca3762af61e59520943dc26494f8941b'):
    print('Invalid password')
    sys.exit()
f=open(join(BASE_DIR,filename),'w')
f.write(text)
f.close()

print('The file has been saved.')
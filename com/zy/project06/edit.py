# C:\Users\Administrator\AppData\Local\Programs\Python\Python36\python.exe


print('Content-type: text/html\n\n')

from os.path import join, abspath
import cgi, sys

#获取目录data的绝对路径
BASE_DIR = abspath('data')
form = cgi.FieldStorage()

filename = form.getvalue('filename')
if not filename:
    print('Please enter a file name')
    sys.exit()
text = open(join(BASE_DIR, filename)).read()
print("""
<html>
 <head>
 <title>Editing...</title>
 </head>
 <body>
 <form action='save.py' method='POST'>
 <b>File:</b> {}<br />
 <input type='hidden' value='{}' name='filename' />
 <b>Password:</b><br />
 <input name='password' type='password' /><br />
 <b>Text:</b><br />
 <textarea name='text' cols='40' rows='20'>{}</textarea><br />
 <input type='submit' value='Save' />
 </form>
 </body>
</html>
""".format(filename, filename, text))

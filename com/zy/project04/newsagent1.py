#!/usr/bin/env python
# -*- coding: utf-8 -*-


'''
一个简单的新闻收集代理
'''

from nntplib import NNTP
#服务器域名
servername='news.gmane.org'
#指定新闻组设置为当前新闻组，并返回一些有关该新闻组的信息
group='gmane.comp.python.committers'
#创建server客户端对象
server=NNTP(servername)
#指定要获取多少篇文章
howmany=10
#返回的值为通用的服务器响应、新闻组包含的消息数、第一条和最后一条消息的编号以及新闻组的名称
resp, count, first, last, name = server.group(group)
start = last-howmany+1

resp,overviews=server.over((start,last))

#从overview中提取主题，并使用ID从服务器获取消息正文
for id,over in overviews:
    subject=over['subject']
    resp,info=server.body(id)
    print(subject)
    print('-'*len(subject))
    for line in info.lines:
        #消息正文行是以字节的方式返回的，但为简单起见，我们直接使用编码Latin-1
        print(line.decode('latin1'))
    print()

#关闭连接
server.quit()


